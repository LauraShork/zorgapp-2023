import java.time.LocalDate;
import java.util.ArrayList;
public class MedicineList
{
    private ArrayList<Medicine>medicineList = new ArrayList<>();

    public MedicineList()
    {
        //
    }

    public void addMedicine(Medicine medicine)
    {
        medicineList.add(medicine);
    }
    public void deleteMedicine()
    {
        System.out.println("Please type the number of the med you want to remove");
        var removeMedIndex = new BScanner( System.in);
        int removeMed  = removeMedIndex.nextInt()-1;
        if (removeMed < medicineList.size())
        {
            medicineList.remove(removeMed);
        }
        else
        {
            System.out.println("Please enter a valid list number");
        }
    }
    public Medicine getMedicine(int medID)
    {
        return medicineList.get(medID);
    }
    public void printAllMeds()
    {
        System.out.format("===================================\n");
        System.out.printf("%-4s %-20s %-20s %s\n", "Id", "Naam", "Type", "Dosering");
        System.out.format( "%s\n", "-".repeat( 80 ) );
        for (int i = 0; i < medicineList.size(); i++)
        {
            Medicine medicine = medicineList.get(i);
            System.out.printf("%-5s", i+1);
            medicine.print();
        }
    }
    public void printAllMedsDate()
    {
        System.out.format("===================================\n");
        System.out.printf("%-4s %-20s %-20s %-25s %s\n", "Id", "Naam", "Type", "Dosering", "Voorschrijf Datum");
        System.out.format( "%s\n", "-".repeat( 80 ) );
        for (int i = 0; i < medicineList.size(); i++)
        {
            Medicine medicine = medicineList.get(i);
            System.out.printf("%-5s", i+1);
            medicine.printDate();
        }
    }
    public void editDosage2()
    {
        var selectMedindex = new BScanner( System.in);
        System.out.println("please enter the list number of the medicine you want to assign a new dosage to");
        int selectedMed = selectMedindex.nextInt()-1;
        for (int i = 0; i < medicineList.size(); i++)
        {
            if (selectedMed == i)
                {
                    System.out.println("please enter the new dosage");
                    Medicine medicine = medicineList.get(i);
                    medicine.editMedDosage();
                }
                else
                {
                    System.out.println("please enter a valid number");
                }
            }
        }
    }

