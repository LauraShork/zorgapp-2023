import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LungMeasurement
{
    private LocalDate measurementDate;
    private double lungMeasurement;

    public LungMeasurement(LocalDate measurementDate, double lungMeasurement)
    {
        this.measurementDate = measurementDate;
        this.lungMeasurement = lungMeasurement;
    }

    public LocalDate getMeasurementDate ()
    {
        return measurementDate;
    }


    public void LMprint()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-LLL-dd");
        String measurementDateFormated = measurementDate.format(formatter);
        int lungMeasurementInt = (int) (lungMeasurement *4);
        System.out.printf("%-15s | %-20s \n", measurementDateFormated, "*".repeat(lungMeasurementInt));
    }
}
