public class GeneralPractitioner extends User
{
    public GeneralPractitioner(int id, String name)
    {
        super(id, name);
        allAppointments.addAppointment(new Appointment("DEFAULT", "Consult", 21.50, null));
        allAppointments.addAppointment(new Appointment("EXTENDED", "Huisbezoek", 43.00, null));
        allAppointments.addAppointment(new Appointment("EXTENDED", "Gezondheidsonderzoek", 43.00, null));
    }
    public boolean weightLengthViewEditPermission()
    {
        return true;
    }
    public boolean canEditMedication()
    {
        return true;
    }
    public String getUserPermission()
    {
        return "gp";
    }
    public void callJob()
    {
        System.out.println("Hello, I am a GP");
    }
}
