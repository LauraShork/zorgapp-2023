public class Dentist extends User
{
    public Dentist(int id, String name)
    {
        super(id, name);
        allAppointments.addAppointment(new Appointment("DEFAULT", "Routune controle", 20.00, null));
        allAppointments.addAppointment(new Appointment("SIMPLE", "Extractie", 30.00, null));
        allAppointments.addAppointment(new Appointment("SIMPLE", "Fluoridebehandeling", 30.00, null));
        allAppointments.addAppointment(new Appointment("COMPLEX", "Wortelkanaalbehandeling", 55.00, null));
        allAppointments.addAppointment(new Appointment("COMPLEX", "Implantaat", 55.00, null));
    }
    public boolean weightLengthViewEditPermission()
    {
        return false;
    }
    public boolean canEditMedication()
    {
        return true;
    }
    public String getUserPermission()
    {
        return "dentist";
    }
    public void callJob()
    {
        System.out.println("Hello, I am a Dentist");
    }
}