import java.util.ArrayList;

public class AppointmentList
{
    private ArrayList<Appointment> appointments = new ArrayList<>();

    public AppointmentList()
    {
        //
    }
    public void addAppointment(Appointment appointment)
    {
        appointments.add(appointment);
    }
    public Appointment getAppointment(int appointmentID)
    {
        return appointments.get(appointmentID);
    }
    public void printAppointment()
    {
        System.out.format("===================================\n");
        System.out.printf("%-4s %-15s %-20s %-25s \n", "ID", "Categorie", "Type", "Tarief");
        System.out.format( "%s\n", "-".repeat( 80 ) );
        for (int i = 0; i < appointments.size(); i++)
        {
            Appointment appointment = appointments.get(i);
            System.out.printf("%-5s", i+1);
            appointment.print();
        }
    }
    public void printAppointmentPlus()
    {
        System.out.format("===================================\n");
        System.out.printf("%-4s %-15s %-20s %-25s \n", "ID", "Categorie", "Type", "Tarief");
        System.out.format( "%s\n", "-".repeat( 80 ) );
        for (int i = 0; i < appointments.size(); i++)
        {
            Appointment appointment = appointments.get(i);
            System.out.printf("%-5s", i+1);
            appointment.print();
        }
    }
    public int size()
    {
        return appointments.size();
    }
}
