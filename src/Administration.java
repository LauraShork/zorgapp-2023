import java.util.Objects;
import java.util.Scanner;
class Administration
{
   private static final int MENUBACK = 0;
   private static final int VIEWPATIENT = 1;
   private static final int VIEWMEDS = 2;
   private static final int SELECTPATIENT = 3;
   private static final int EDITPATIENT = 4;
   private static final int VIEWALLPATIENTS = 5;
   private static final int VIEWALLMEDS = 6;
   private static final int SETNEWMED = 7;
   private static final int SETMEDDOSAGE = 8;
   private static final int DELETEMEDICINELIST = 9;
   private static final int EDITDEFAULTDOSAGE = 10;
   private static final int SHOWALLAPOINTMENTS = 11;
   private static final int ADDAPPOINTMENT = 12;
   private static final int VIEWAPPOINTMENTS = 13;
   private static final int ADDLUNGMEASUREMENT = 14;
   private static final int SHOWLUNGMEASURMENT = 15;
   private static final int SECRETDEVMENU = 621;
   private static final int CALLJOB = 1;
   private static final int VIEWALLUSERS = 2;

   private Patient currentPatient;                                                                                      // The currently selected patient
   private User    currentUser;                                                                                         // the current user of the program.
   private MedicineList allMedicine = new MedicineList();                                                               //fixed list of all meds

   Administration( User user )
   {
      currentUser    = user;
      Patientlist patientlist = new Patientlist();
      currentPatient = patientlist.selectPatient(currentUser);

      allMedicine.addMedicine(new Medicine(1,"Ibuprofen", "Painkiller", "3 times 2mg per day"));
      allMedicine.addMedicine(new Medicine(2, "Thymol", "Anti fungal", "1 times 2mg per day"));
      allMedicine.addMedicine(new Medicine(3, "(Es)omeprazol ", "maagzuurremmer", "1 times 5mg per day"));
      allMedicine.addMedicine(new Medicine(4, "Jodium", "Schildklier hormone", "1 times 4mg per day"));
      allMedicine.addMedicine(new Medicine(5, "Paracetamol", "Painkiller", "1 times 2mg per day"));
      allMedicine.addMedicine(new Medicine(6, "Amylmetacresol", "Antiseptic", "1 times 0.5mg per day"));

      currentPatient.addMedicine();
   }
   public void menu()
   {
      Scanner scanner = new Scanner( System.in );                                                                       // User input via this scanner. todo, use BScanner
      Patientlist patientlist = new Patientlist();

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.format( "%s\n", "=".repeat( 80 ) );
         System.out.printf("Current user type %s\n", currentUser.getUserPermission());
         System.out.format( "Current patient: %s\n", currentPatient.fullName() );
         System.out.format( "%d:  STOP\n", MENUBACK );
         System.out.format( "%d:  View patient data\n", VIEWPATIENT );
         System.out.format( "%d:  list the medicine of the current patient\n", VIEWMEDS);
         System.out.format( "%d:  select patients\n", SELECTPATIENT);
         System.out.format( "%d:  edit selected patient\n", EDITPATIENT);
         System.out.format( "%d:  list all patients\n", VIEWALLPATIENTS);
         System.out.format( "%d:  list all medicines\n", VIEWALLMEDS);
         System.out.format( "%d:  Set a new medicine for the selected patient\n", SETNEWMED);

         if (currentUser.canEditMedication())
         {
         System.out.format( "%d:  Set a new dosage for a patient's medicine\n", SETMEDDOSAGE);
         }

         System.out.format( "%d:  Remove a medicine from the patient's medicine list\n", DELETEMEDICINELIST);
         System.out.format( "%d:  Edit the default dosage of a medicine\n", EDITDEFAULTDOSAGE);
         System.out.format( "%d:  Show all appointments\n", SHOWALLAPOINTMENTS);
         System.out.format( "%d:  Make a new appointment for the selected patient\n", ADDAPPOINTMENT);
         System.out.format( "%d:  Show the created appointments\n", VIEWAPPOINTMENTS);
         System.out.printf( "%d:  Add a new lung measurement\n", ADDLUNGMEASUREMENT);
         System.out.printf( "%d:  display the taken lung measurements\n", SHOWLUNGMEASURMENT);

         System.out.print( "enter #choice: " );
         int choice = scanner.nextInt();
         switch (choice)
         {
            case MENUBACK:
               nextCycle = false;
               break;

            case VIEWPATIENT:
               currentPatient.viewData(currentUser.weightLengthViewEditPermission());
               break;

            case VIEWMEDS:
               currentPatient.patientMedicine.printAllMedsDate();
               break;

            case SELECTPATIENT:
               currentPatient = patientlist.selectPatient(currentUser);
               System.out.println("patient selected");
               currentPatient.addMedicine();
               break;

            case EDITPATIENT:
               currentPatient.editData(currentUser.canEditMedication());
               break;

            case VIEWALLPATIENTS:
               patientlist.viewAllPatients();
               break;

            case VIEWALLMEDS:
               allMedicine.printAllMeds();
               break;

            case SETNEWMED:
               allMedicine.printAllMeds();
               currentPatient.editMedID();
               currentPatient.addMedicine();
               break;

            case DELETEMEDICINELIST:
               currentPatient.patientMedicine.printAllMeds();
               currentPatient.patientMedicine.deleteMedicine();
               break;

            case SETMEDDOSAGE:
               if (currentUser.canEditMedication()) {
               currentPatient.patientMedicine.printAllMeds();
               currentPatient.patientMedicine.editDosage2();}
               else {System.out.println( "Please enter a *valid* digit" );}
               break;

            case EDITDEFAULTDOSAGE:
               allMedicine.printAllMeds();
               allMedicine.editDosage2();
               break;

            case SHOWALLAPOINTMENTS:
               currentPatient.allAppointments.printAppointment();
               break;

            case ADDAPPOINTMENT:
               currentUser.allAppointments.printAppointment();
               currentPatient.editAppointmentID(currentUser);
               currentPatient.addAppointment(currentUser);
               break;

            case VIEWAPPOINTMENTS:
               currentPatient.newAppointment.printAppointmentPlus();
               break;

            case ADDLUNGMEASUREMENT:
               currentPatient.addMeasurement();
               break;

            case SHOWLUNGMEASURMENT:
               currentPatient.printMeasurement();
               break;

            case SECRETDEVMENU:

               boolean nextCycle2 = true;
               while (nextCycle2)
               {
                  System.out.format( "%s\n", "=".repeat( 80 ) );
                  System.out.println("You are not supposed be be here, please leave");
                  System.out.format( "%s\n", "=".repeat( 80 ) );
                  System.out.format( "%d:  STOP\n", MENUBACK );
                  System.out.format( "%d:  Who are you?\n", CALLJOB);
                  System.out.format( "%d:  list all users\n", VIEWALLUSERS);

                  var selectSecret = new BScanner( System.in);
                  int secretSelect = selectSecret.nextInt();
                  switch (secretSelect)
                  {
                     case MENUBACK:
                        nextCycle2 = false;
                        break;

                     case CALLJOB:
                        currentUser.callJob();
                        break;

                     case VIEWALLUSERS:
                        Userlist userlist = new Userlist();
                        userlist.printUsers();
                        break;

                     default:
                        System.out.println( "Please enter a *valid* digit" );
                        break;
                  }

               }

            default:
               System.out.println( "Please enter a *valid* digit" );
               break;
         }
      }
   }
}
