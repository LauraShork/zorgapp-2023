import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Medicine
{
    private String name;
    private String type;
    private String dosage;
    private int id;
    private LocalDate prescriptionDate;

    public Medicine(int id, String name, String type, String dosage, LocalDate prescriptionDate)
    {
        this.name = name;
        this.type = type;
        this.dosage = dosage;
        this.id = id;
        this.prescriptionDate = prescriptionDate;
    }
    public Medicine(int id, String name, String type, String dosage)
    {
        this.name = name;
        this.type = type;
        this.dosage = dosage;
        this.id = id;
        //prescriptionDate = LocalDate.now();
    }
    public Medicine(Medicine medicine)
    {
        this.name = medicine.name;
        this.type = medicine.type;
        this.dosage = medicine.dosage;
        this.id = medicine.id;
        prescriptionDate = LocalDate.now();
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDosage() {
        return dosage;
    }
    public LocalDate getPrescriptionDate() {
        return prescriptionDate;
    }

    public void setDosage(String dosage)
    {
        this.dosage = dosage;
    }
    public void editMedDosage()
    {
        var medDosage = new BScanner(System.in);
        setDosage(medDosage.nextLine());
    }
    public void print()
    {
        System.out.format("%-20s %-20s %s \n", getName(), getType(), getDosage());
    }
    public void printDate()
    {
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("d/MM/uuuu");
        String formattedString = getPrescriptionDate().format(formatters);
        System.out.format("%-20s %-20s %-25s %s \n", getName(), getType(), getDosage(), formattedString);
    }
}
