import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

class Patient
{
   private static final int RETURN      = 0;
   private static final int SURNAME     = 1;
   private static final int FIRSTNAME   = 2;
   private static final int DATEOFBIRTH = 3;
   private static final int WEIGHT = 4;
   private static final int LENGTH = 5;

   private final int    id;
   private String       surname;
   private String       firstName;
   private LocalDate    dateOfBirth;
   private final LocalDate currentDate;
   private long         age;
   private double       height;
   private double       weight;
   private double       bmi;
   private String       bmiText;
   private int          medID;
   private int          appointmentID;
   private MedicineList allMedicine = new MedicineList();
   public AppointmentList allAppointments = new AppointmentList();

   public void setWeight(double weight) {
      this.weight = weight;
   }
   public void setHeight(double height) {this.height = height; }
   public void medIDSet(int medID) {
      this.medID = medID;
   }
   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }
   public void setDateOfBirth(String dateOfBirth) {
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      this.dateOfBirth = LocalDate.from(dtf.parse(dateOfBirth));
   }
   public void setSurname(String surname) {
      this.surname = surname;
   }
   public int getMedID() {
      return medID;
   }
   public int getAppointmentID() {
      return appointmentID;
   }


   // Constructor-

   public Patient(int id, String surname, String firstName, LocalDate dateOfBirth, double weight, double height, int medID)
   {
      this.id           = id;
      this.surname      = surname;
      this.firstName    = firstName;
      this.dateOfBirth  = dateOfBirth;
      currentDate       = LocalDate.now();
      this.age          = calcage();
      this.weight       = weight;
      this.height       = height;
      this.bmi          = calcBMI();
      this.medID        = medID;
      bmiText(bmi);
      allMedicine.addMedicine(new Medicine(1,"Ibuprofen", "Painkiller", "3 times 2mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(2, "Thymol", "Anti fungal", "1 times 2mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(3, "(Es)omeprazol ", "maagzuurremmer", "1 times 5mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(4, "Jodium", "Schildklier hormone", "1 times 4mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(5, "Paracetamol", "Painkiller", "1 times 2mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(6, "Amylmetacresol", "Antiseptic", "1 times 0.5mg per day", LocalDate.now()));
   }
   public Patient(int id, String surname, String firstName, LocalDate dateOfBirth, double weight, double height)
   {
      this.id           = id;
      this.surname      = surname;
      this.firstName    = firstName;
      this.dateOfBirth  = dateOfBirth;
      currentDate       = LocalDate.now();
      this.age          = calcage();
      this.weight       = weight;
      this.height       = height;
      this.bmi          = calcBMI();
      bmiText(bmi);
      allMedicine.addMedicine(new Medicine(1,"Ibuprofen", "Painkiller", "3 times 2mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(2, "Thymol", "Anti fungal", "1 times 2mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(3, "(Es)omeprazol ", "maagzuurremmer", "1 times 5mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(4, "Jodium", "Schildklier hormone", "1 times 4mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(5, "Paracetamol", "Painkiller", "1 times 2mg per day", LocalDate.now()));
      allMedicine.addMedicine(new Medicine(6, "Amylmetacresol", "Antiseptic", "1 times 0.5mg per day", LocalDate.now()));


   }
   void viewData(boolean permission)
   {
      DateTimeFormatter formatters = DateTimeFormatter.ofPattern("d/MM/uuuu");
      String dateOfBirthEU = dateOfBirth.format(formatters);
      System.out.format( "===== Patient id=%d ==============================\n", id );
      System.out.format( "%-17s %s\n", "Surname:", surname );
      System.out.format( "%-17s %s\n", "firstName:", firstName );
      System.out.format( "%-17s %s\n", "Date of birth:", dateOfBirthEU );
      System.out.printf( "%-17s %s years\n", "Age:", age);

      if (permission){
      System.out.printf( "%-17s %s m\n", "Height:", height);
      System.out.printf( "%-17s %s kg\n", "Weight:", weight);
      System.out.printf( "%-17s %.2f kg/m2\n", "BMI:", bmi);
      System.out.printf( "%-17s %s\n", "BMI score", bmiText);}
   }

   public MedicineList patientMedicine = new MedicineList();
   public void addMedicine()
   {
         patientMedicine.addMedicine(new Medicine(allMedicine.getMedicine(medID)));
   }
   public void editData(boolean permission)
   {
      var scanner = new BScanner(System.in);
      boolean nextCycle2 = true;
      while (nextCycle2)
      {
         System.out.printf("%d: Return\n", RETURN);
         System.out.printf("%d: SURNAME\n", SURNAME);
         System.out.printf("%d: FIRSTNAME\n", FIRSTNAME);
         System.out.printf("%d: DATEOFBIRTH\n", DATEOFBIRTH);
         if (permission){
            System.out.printf("%d: WEIGHT\n", WEIGHT);
            System.out.printf("%d: LENGTH\n", LENGTH);
         }

         System.out.print( "enter #choice: " );

         int choice1 = scanner.nextInt();
         switch (choice1)
         {
            case RETURN:
               nextCycle2 = false;
               break;

            case SURNAME:
               System.out.println("Please type a new surname: ");
               var editScannerSurname = new BScanner( System.in);
               setSurname(editScannerSurname.nextLine());
               break;

            case FIRSTNAME:
               System.out.println("Please type a new firstname: ");
               var editScannerfirstname = new BScanner( System.in);
               setFirstName(editScannerfirstname.nextLine());
               break;

            case DATEOFBIRTH:
               System.out.println("Please type a new date of birth (yyyy-mm-dd): ");
               var editScannerdateofbirth = new BScanner( System.in);
               setDateOfBirth(editScannerdateofbirth.nextLine());
               this.age = calcage();
               checkDateOfBirth();
               break;

            case WEIGHT:
               if (permission){
               System.out.println("Please type a new weight in kilograms (xx.xx): ");
               var editScannerweight = new BScanner( System.in);
               setWeight(editScannerweight.nextDouble());
               this.bmi = calcBMI();}
               else {System.out.println( "Please enter a *valid* number" );}
               break;

            case LENGTH:
               if (permission){
               System.out.println("Please type a new Length: ");
               var editScannerLength = new BScanner( System.in);
               setHeight(editScannerLength.nextDouble());
               this.bmi = calcBMI();}
               else {System.out.println( "Please enter a *valid* number" );}
               break;

            default:
               System.out.println( "Please enter a *valid* number" );
               break;
         }
      }
   }

   public void editMedID()
   {
      System.out.println("please enter the number of the medicine you want to assign");
      var medIDEdit = new BScanner(System.in);
      int inbetweenInt = medIDEdit.nextInt()-1;
         if (inbetweenInt < 6)
         {
            medIDSet(inbetweenInt);
         }
         else
         {
            System.out.println("please enter a valid number");
            editMedID();
         }
   }

   public AppointmentList newAppointment = new AppointmentList();
   public void setAppointmentID(int appointmentID)
   {
      this.appointmentID = appointmentID;
   }
   public void addAppointment(User currentUser)
   {
      newAppointment.addAppointment(new Appointment(currentUser.allAppointments.getAppointment(appointmentID)));
   }
   public void editAppointmentID(User user)
   {
      System.out.println("please enter the number of the appointment type you want to assign");
      var medIDEdit = new BScanner(System.in);
      int inbetweenInt = medIDEdit.nextInt()-1;
      if (inbetweenInt < user.allAppointments.size())
      {
         setAppointmentID(inbetweenInt);
      }
      else
      {
         System.out.println("please enter a valid number");
         editAppointmentID(user);
      }
   }

   private ArrayList<LungMeasurement> lungMeasurements = new ArrayList<>();

   public void addMeasurement()
   {
      int i = lungMeasurements.size() -1;
      if (i != -1) {
         if (LocalDate.now().isAfter(lungMeasurements.get(i).getMeasurementDate())) {
            System.out.println("Please enter the measured lung-capacity of the patient");
            var takenMeasurement1 = new BScanner(System.in);
            double takenMeasurement = takenMeasurement1.nextDouble();
            lungMeasurements.add(new LungMeasurement(LocalDate.now(), takenMeasurement));
         } else {
            System.out.println("you can only make one measurement a day");
         }
      }
      else
      {
         System.out.println("Please enter the measured lung-capacity of the patient");
         var takenMeasurement1 = new BScanner(System.in);
         double takenMeasurement = takenMeasurement1.nextDouble();
         lungMeasurements.add(new LungMeasurement(LocalDate.now(), takenMeasurement));
      }
   }
   public void printMeasurement()
   {
      System.out.printf("%-15s %-20s\n", "Meetdatum", "Longinhoud (0.25L = *)");
      System.out.printf("%s \n", "-".repeat(30));
      for (LungMeasurement lungMeasurement : lungMeasurements)
      {
         lungMeasurement.LMprint();
      }
   }

   long calcage()
   {
      return ChronoUnit.YEARS.between(this.dateOfBirth, currentDate);
   }
   double calcBMI()
   {
      return this.weight/(this.height*this.height);
   }

   public void checkDateOfBirth()
   {
      if (this.age < 0)
      {
         var editDOBscanner = new BScanner(System.in);
         System.out.println("Please enter a valid date of birth (yyyy-mm-dd)");
         setDateOfBirth(editDOBscanner.nextLine());
         this.age = calcage();
         checkDateOfBirth();
      }
      else {System.out.println("The date of birth has been changed");}
   }

   public void bmiText(double boMaIn)
   {
      this.bmi = boMaIn;
      if (this.age >= 70) {
         if (boMaIn <= 21.9) {
            bmiText = "This person has a slim bmi status";
         } else if (boMaIn <= 28) {
            bmiText = "This person has a good bmi status";
         } else if (boMaIn <= 29.9) {
            bmiText = "This person has a heavy bmi status";
         } else {
            bmiText = "This person has a to heavy bmi status";
         }
      } else {
         if (boMaIn <= 16.9) {
            bmiText = "This person has a to slim bmi status";
         } else if (boMaIn <= 18.4) {
            bmiText = "This person has a slim bmi status";
         } else if (boMaIn <= 25) {
            bmiText = "This person has a good bmi status";
         } else if (boMaIn <= 29.9) {
            bmiText = "This person has a heavy bmi status";
         } else {
            bmiText = "This person has a to heavy bmi status";
         }
      }
   }
   String fullName()
   {
      DateTimeFormatter formatters = DateTimeFormatter.ofPattern("d/MM/uuuu");
      String dateOfBirthEU = dateOfBirth.format(formatters);
      return String.format( "%s %s [%s]", firstName, surname, dateOfBirthEU);
   }
}
