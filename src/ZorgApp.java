class ZorgApp {
   public static void main(String[] args) {
      Userlist userlist = new Userlist();                            //initialises user
      User user = userlist.selectUser();                             //makes a new object User with the variable name userVar, calls method loginUser
      Administration administration = new Administration(user);      //makes a new object Administration with the variable name administration
      administration.menu();                                         //displays the menu using the method "menu" in the var administration
   }
}