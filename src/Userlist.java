import java.util.ArrayList;

public class Userlist
{
    private ArrayList<User> userArrayList = new ArrayList<>();
    public Userlist()
    {
        userArrayList.add(new Fysiotherapeut(1, "Mark ElCamera"));
        userArrayList.add(new Dentist(2, "Hen Krull"));
        userArrayList.add(new GeneralPractitioner(3, "Bob van Eisden"));
    }
    public User selectUser() {                                                                                          //method which calls the Arraylist of User objects
        System.out.printf("%s\n", "=".repeat(80));                                                                //prints menu
        var changeScannerUser = new BScanner(System.in);

        for (int i = 0; i < userArrayList.size(); i++) {                                                                //loops through arraylist
            User user = userArrayList.get(i);
            System.out.println(i +1+ " " + user.getUserName());                                                         //displaying the username's and id's
        }
        System.out.println("Please enter User ID: ");
        int usr = changeScannerUser.nextInt() -1;                                                                       //fetches user input for ID

        if (usr > userArrayList.size() - 1) {
            System.out.println("Please enter valid ID");                                                                //checks if input is within array
            selectUser();
        } else {
            return userArrayList.get(usr);                                                                              //returns the input userID for the output
        }
        return userArrayList.get(usr);
    }
    public void printUsers() {
      for (int i = 0; i < userArrayList.size(); i++) {
         User user = userArrayList.get(i);
         System.out.println(i + 1 + " " + user.getUserName());
      }
   }
}
