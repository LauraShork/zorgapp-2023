import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Appointment
{
    private String      tariefCatagory;
    private String      tariefType;
    private double      rate;
    private LocalDate   appointmentDate;


    public Appointment(String tariefCatagory, String tariefType, double rate, LocalDate appointmentDate)
    {
        this.tariefCatagory = tariefCatagory;
        this.tariefType = tariefType;
        this.rate = rate;
        this.appointmentDate = appointmentDate;
    }

    public Appointment(Appointment appointment)
    {
        this.tariefCatagory = appointment.tariefCatagory;
        this.tariefType = appointment.tariefType;
        this.rate = appointment.rate;
    }

    public LocalDate getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDateD(String appointmentDate) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        this.appointmentDate = LocalDate.from(dtf.parse(appointmentDate));
    }

    public String getTariefCatagory() {
        return tariefCatagory;
    }

    public String getTariefType() {
        return tariefType;
    }

    public double getRate() {
        return rate;
    }

    public void SetAppointmentDate()
    {
        System.out.println("Please enter the date of the appointment (yyyy-mm-dd): ");
        var appointmentDateChanger = new BScanner(System.in);
        setAppointmentDateD(appointmentDateChanger.nextLine());
    }

    public void print()
    {
        System.out.printf("%-15s %-25s %-20s \n", getTariefCatagory(), getTariefType(), getRate());
    }
    public void printPlusDate()
    {
        System.out.printf("%-15s %-25s %-20s %-20s \n", getTariefCatagory(), getTariefType(), getRate(), getAppointmentDate());
    }
}
