import java.time.LocalDate;
import java.util.ArrayList;

public class Patientlist {
    public ArrayList<Patient> patientArrayList = new ArrayList<>();

    Patientlist() {
        patientArrayList.add(new Patient(1, "Van Puffelen", "Pierre", LocalDate.of(2000, 2, 29), 80, 1.85,1));
        patientArrayList.add(new Patient(2, "Smit", "Laura", LocalDate.of(1996, 8, 17), 76, 1.86));
        patientArrayList.add(new Patient(3, "Van der Veer", "Wyske", LocalDate.of(1995, 9, 12), 81, 1.90 ));
        patientArrayList.add(new Patient(4, "Schuurman", "margreet", LocalDate.of(1973, 4, 18), 59, 1.62));
        patientArrayList.add(new Patient(5, "Blow", "Richard", LocalDate.of(1995, 11, 3), 91, 1.95, 5));
    }

    public Patient selectPatient(User user) {
        System.out.printf("%s\n", "=".repeat(80));                                                                //Prints patient select menu
        System.out.format("Current user: [%d] %s\n", user.getUserID(), user.getUserName());
        System.out.printf("%s\n", "=".repeat(80));
        System.out.println("Please select patient");
        System.out.printf("%s\n", "=".repeat(80));

        for (int i = 0; i < patientArrayList.size(); i++)
        {
            Patient patient = patientArrayList.get(i);
            System.out.println(i + 1 + " " + patient.fullName());
        }

        System.out.println("Please enter patient ID: ");
        var changeScanner = new BScanner(System.in);
        int pat = changeScanner.nextInt() - 1;

        if (pat > patientArrayList.size()){
            System.out.println("Please enter valid ID");
            selectPatient(user);
        }
        return patientArrayList.get(pat);
    }

    public void viewAllPatients() {
        for (int i = 0; i < patientArrayList.size(); i++) {
            Patient patient = patientArrayList.get(i);
            System.out.println(i + 1 + " " + patient.fullName());
        }
    }
}
