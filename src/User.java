 class User
{
   public AppointmentList allAppointments = new AppointmentList();
   String userName;
   int    userID;
   String getUserName()
   {
      return userName;
   }

   int getUserID()
   {
      return userID;
   }
   public User( int id, String name)
   {
      this.userID   = id;
      this.userName = name;
   }
   public boolean weightLengthViewEditPermission()
   {
      return false;
   }
   public boolean canEditMedication()
   {
      return false;
   }
   public String getUserPermission()
   {
      return "null";
   }
   public void callJob()
   {
      //
   }
}
