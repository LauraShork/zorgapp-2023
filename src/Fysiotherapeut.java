public class Fysiotherapeut extends User
{
    public Fysiotherapeut(int id, String name)
    {
        super(id, name);
        allAppointments.addAppointment(new Appointment("DEFAULT", "Standaard behandeling", 17.50, null));
        allAppointments.addAppointment(new Appointment("SHORT", "Tapen en bandageren", 25.00, null));
        allAppointments.addAppointment(new Appointment("SHORT", "Mobilisatie", 25.00, null));
        allAppointments.addAppointment(new Appointment("SHORT", "Massage", 25.00, null));
        allAppointments.addAppointment(new Appointment("EXTENDED", "Manuele therapie", 50.00, null));
        allAppointments.addAppointment(new Appointment("EXTENDED", "Dry needling", 50.00, null));
        allAppointments.addAppointment(new Appointment("FACILITIES", "Gebruik van oefenbad", 5.00, null));
    }
    public boolean weightLengthViewEditPermission()
    {
        return true;
    }
    public boolean canEditMedication()
    {
        return false;
    }
    public String getUserPermission()
    {
        return "fysio";
    }
    public void callJob()
    {
        System.out.println("Hello, I am a Fysio");
    }
}